# mosaik-thesis-topics

In this project we collect ideas for thesis topics around mosaik and co-simulation. To find the topics, just go the the [Issue Board](https://gitlab.com/mosaik/mosaik-thesis-topics/-/boards).

